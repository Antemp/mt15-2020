## Créer un compte gitlab

Rendez-vous sur https://gitlab.com.

Créez un compte en cliquant sur "Register now" en prenant de choisir un mot de passe que vous retiendrez facilement, car vous en aurez besoin.

Ne __PAS__ créer un compte en se connectant avec google par exemple, cela empêche les sauvegardes!!!


## Forker le projet contenant les tp

Connectez-vous sur votre compte gitlab, puis cliquez sur le lien:

https://gitlab.com/utt-connected-innovation/mt15-2020/

<img src="data/projet.png">

Cliquez sur "Fork" en haut à droite

<img src="data/fork.png">

Cliquez sur le namespace qui correspond à votre nom.

Vous avez récupéré le projet contenant les tp sur votre compte gitlab.

Copiez l'url de la page qui s'affiche. __Ce n'est PAS__ https://gitlab.com/utt-connected-innovation/mt15-2020/.

L'url doit être https:<span>//gitlab.com/###votre_pseudo###/mt15-2020/</span>

## Lancer le projet sur binder

Cliquez sur https://mybinder.org/

<img src="data/binder.png">

Selectionnez "GitLab.com" à la place "GitHub" dans la premier menu

Puis coller l'url copiée précédemment dans la première ligne du formulaire.

Cliquez enfin sur "launch".

Après quelques minutes, l'environnement de travail doit être chargé et prêt à l'emploi.

<img src="data/tps.png">

## Sauvegarder les modifications

Toutes les modifications que vous allez faire seront perdues si on ne les transmet pas au projet gitlab.

Pour éviter cela, nous allons préparer l'envoi des modifications effectuées sur votre projet gitlab.

Cliquez sur __"New"__ en haut à droite, puis __"Terminal"__

<img src="data/new.png">

Une fois dans le terminal il faut taper en remplaçant par les bonnes valeurs:

```bash
git config --global user.email "mail@example.com"

git config --global user.name "mon_nom"

```
<img src="data/Terminal.png">


Une fois que vous aurez effectuer des modifications et que vous les aurez enregistrées ( je vous conseille de le faire régulièrement), revenez sur le terminal et taper:

```bash
git commit -am "message"
git push
```

Le terminal vous demandera vos identifiants GitLab.

Ainsi les modifications sont envoyées à votre projet gitlab. Donc même en cas de deconnexion, il suffira de reprendre la procédure à partir de __"lancer le projet sur binder"__

Si jamais vous voulez ajouter des fichiers, il faudra d'abord taper :
```bash
git add le_nom_du_fichier
```

AVANT de taper le __git commit__